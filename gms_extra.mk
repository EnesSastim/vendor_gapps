# product/app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Drive \
    Maps \
    Photos \
    PixelWallpapers2022 \
    PrebuiltGmail \
    Videos \
    Youtube \
    YoutubeMusicPrebuilt \
    talkback

# product/priv-app
PRODUCT_PACKAGES += \
    GoogleCamera \
    PixelLiveWallpaperPrebuilt \
    RecorderPrebuilt \
    ScribePrebuilt \
    SecurityHubPrebuilt \
    Showcase \
    TipsPrebuilt \
    Velvet


