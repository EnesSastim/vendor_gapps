PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true

GMS_DEVICE := $(subst aosp_,,$(TARGET_PRODUCT))
ifneq ($(filter walleye taimen, $(GMS_DEVICE)),)
$(call inherit-product, vendor/gapps/device/pixel2.mk)
else ifneq ($(filter bonito sargo crosshatch blueline, $(GMS_DEVICE)),)
$(call inherit-product, vendor/gapps/device/pixel3.mk)
else
$(call inherit-product, vendor/gapps/gms_extra.mk)
endif

$(call inherit-product, vendor/gapps/gms.mk)
