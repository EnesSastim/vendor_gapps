# product/app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Photos

# product/priv-app
PRODUCT_PACKAGES += \
    GoogleCamera \
    ScribePrebuilt \
    SecurityHubPrebuilt \
    Velvet
